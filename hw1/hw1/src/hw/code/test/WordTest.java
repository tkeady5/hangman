package hw.code.test;

import static org.junit.Assert.assertEquals;
import hw.code.Word;

import org.junit.Before;
import org.junit.Test;

public class WordTest {
  
    Word w1;
    // word is "test"
  
    @Before
    public void setup() {
        w1 = new Word("test");
    }
      
  /**
   * to test the constructor, check the word returned
   * and the occurrences array.
   * This will also test getOccurrences and getWord
   */
  @Test
  public void testConstruct() {
    //first check the actual word String
    assertEquals("test", w1.getWord());
    //might as well test getSize while at it
    assertEquals(4, w1.getSize());
    //check occurrences array
    //t is 20th letter (19 index). 2 t's
    //e is the 5th letter (4 index). 1 e
    //s is the 19th letter (18 index). 1 s
    for (char c = 'a'; c < ((int) 'a' + 26); c++) {
      if (c == 't') {
        assertEquals(2, w1.getOccurrences(c));
      } else if ((c == 'e') || (c == 's')) {
        assertEquals(1, w1.getOccurrences(c));
      } else {
        assertEquals(0, w1.getOccurrences(c));
      }
    }
    //now test extremes if user inputs incorrect input
    //should just return 0... should be handled further up
    //but just in case will cost the user a guess.
    assertEquals(0, w1.getOccurrences('A'));
    assertEquals(0, w1.getOccurrences('0'));
  }
  
  /**
   * test letterFinder will test to see if a binary encoding
   * number that represents the locations of the letters. 
   */
  @Test
  public void testLetterFinder() {
    // will test for each letter in the word
    assertEquals(9, w1.letterFinder('t'));
    assertEquals(2, w1.letterFinder('s'));
    assertEquals(4, w1.letterFinder('e'));
    
    // a letter not in the word
    assertEquals(0, w1.letterFinder('b'));
    // a special character that won't exist
    assertEquals(0, w1.letterFinder('!'));
    
  }

}

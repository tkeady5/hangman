package hw.code.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import hw.code.Dictionary;
import hw.code.Word;

import org.junit.Before;
import org.junit.Test;

public class DictionaryTest {
    
    Dictionary first;
    Dictionary second;
    Dictionary third;
    
    @Before
    public void setup() {
        first = new Dictionary("C:\\Users\\Nitin\\Documents\\JohnsHopkins\\Fall2015\\DataStructures\\gitRepo\\hw1\\hw1\\src\\hw\\code\\test\\lengthsTest.txt");      // Has lengths 1 - 26
        second = new Dictionary("C:\\Users\\Nitin\\Documents\\JohnsHopkins\\Fall2015\\DataStructures\\gitRepo\\hw1\\hw1\\src\\hw\\code\\test\\oneOrThree.txt");      // Has 1 3 letter word, 3 5 letter words
        third = new Dictionary("C:\\Users\\Nitin\\Documents\\JohnsHopkins\\Fall2015\\DataStructures\\gitRepo\\hw1\\hw1\\src\\hw\\code\\test\\oneWord.txt");
    }
    
    
    @Test
    public void testConstruct() {
        assertEquals(26, first.getSize());
        assertEquals(2, second.getSize());
        ArrayList<Word> picked = second.getWordList();          // Grab a random one
        
        if (picked.size() == 1) {
            assertEquals("one", picked.get(0));
        } else {
            assertEquals(3, picked.size());
            assertEquals("three", picked.get(0));   // they should stay in order right?
            assertEquals("eerht", picked.get(1));
            assertEquals("theer", picked.get(2));
        }
        
        assertEquals(1, third.getSize());
        assertEquals(1, third.getWordList().size());
        
    }
    

}

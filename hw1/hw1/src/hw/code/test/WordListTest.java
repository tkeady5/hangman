package hw.code.test;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import hw.code.Word;
import hw.code.WordList;

public class WordListTest {

    // Words for the list
    Word w1, w2, w3, w4, w5, w6, w7, w8, w9;
    
    // WordList
    WordList testList;
    
    // result ArrayList
    ArrayList<Word> result;
    
    // Contains result
    WordList resultList;
    
    // More testing
    WordList secondList;
    
    // All the testing
    ArrayList<Word> secondReturn;
    
    // you get the idea
    WordList thirdList;
    
    ArrayList<Word> thirdReturn;
    
    @Before
    public void setup() {
        w1 = new Word("test");
        w2 = new Word("step");
        w3 = new Word("list");
        ArrayList<Word> arrayList = new ArrayList<Word>();
        arrayList.add(w1);
        arrayList.add(w2);
        arrayList.add(w3);
        testList = new WordList(arrayList);
        
        result = new ArrayList<Word>();
        result.add(w1);
        result.add(w3);
        resultList = new WordList(result);
        testList.sort('s');
        
        
        w4 = new Word("achoo"); // Should spit
        w5 = new Word("yahoo"); // out these 
        w6 = new Word("garoo"); // three from secondList
        w7 = new Word("oovoo"); // Should spit
        w8 = new Word("oodoo"); // out these
        w9 = new Word("lolol"); // three from thirdList
        ArrayList<Word> second = new ArrayList<Word>();
        second.add(w4);
        second.add(w5);
        second.add(w6);
        second.add(w7);
        second.add(w8);
        second.add(w9);
        secondList = new WordList(second);
        secondList.sort('o');
        
        secondReturn = new ArrayList<Word>();
        secondReturn.add(w4);
        secondReturn.add(w5);
        secondReturn.add(w6);
        
        
        ArrayList<Word> third = new ArrayList<Word>();
        third.add(w4);
        third.add(w5);
        third.add(w6);
        third.add(w7);
        third.add(w8);
        third.add(w9);
        thirdList = new WordList(third);
        thirdList.sort('a');
        
        thirdReturn = new ArrayList<Word>();
        thirdReturn.add(w7);
        thirdReturn.add(w8);
        thirdReturn.add(w9);
        
        System.out.println(third);
    }
    
    @Test
    public void testSort() {
        assertEquals(1, testList.sort('t'));
        assertEquals(0, testList.sort('e'));
        assertEquals(1, testList.sort('s'));
    }
    
    
    @Test
    public void testChoose() {
        
        assertEquals(result, testList.choose('s'));
        assertEquals(secondReturn, secondList.choose('o'));
        assertEquals(thirdReturn, thirdList.choose('a'));
    }
    
    
}

package hw.code;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class WordList- This list of words will be used throughout the game.
 * This class will have functions to organize and query
 * the words, and decide which words to keep based on a
 * user's guess.
 * @author Nitin and Thomas
 *
 */

public class WordList {
  
    /**
     * startList is the list that is initially passed in from
     * the previous turn and will be worked on and organized.
     */
    public ArrayList<Word> startList;
    /**
     * occurrences is the sorted list of words
     * based on the number of occurrences of a
     * given letter in each word.
     */
    private ArrayList<Word> occurrences;
  
    /**
     * int wordSize is the length of the words in this
     * list.
     */
    private int wordSize;
    
    /**
     * Constructor WordList will simply set the startList pointer
     * equal to whatever was the just created ArrayList of words.
     * Also records size of words in list.
     * @param inputList - this is the ArrayList of words in play.
     */
    public WordList(ArrayList<Word> inputList) {
        this.startList = inputList;
        this.wordSize = this.startList.get(0).getSize();
        this.occurrences = new ArrayList<Word>();
    }
    
    /**
     * Function to return length of words for game.
     * @return wordSize - returns length of words in the list
     */
    public int getWordSize() {
        return this.wordSize;
    }
    
    /**
     * Function to return a requested word from the startList (added by Thomas).
     * @param index - selector for which word inside startList must be used
     * @return Word - The requested word inside the internal ArrayList<Word>
     */
    public Word get(int index) {
        return this.startList.get(index);
    }
    
    /**
     * Function to return the length of the list.
     * @return int - returning size of the startList
     */
    public int size() {
        return this.startList.size();
    }
    
    /**
     * sorts function takes in a character from the user
     * and searches for any occurrences of the letter in
     * the words.
     * @param toFind - character just guessed by the user.
     * @return minimum - int that is the min characters in 
     */
    public int sort(char toFind) {
        // declare a watcher var to keep track of min occurrences of letter
        int minimum;
        // have an initial run to set a minimum value
        int i = 0;
        
        // HAVE TO CLEAR OCCURRENCES EACH TIME right???
        this.occurrences.clear();  // not if they are set to the output
        // of choose() (<- this is false)
        
        Word tempWord = this.startList.get(i);
        minimum = tempWord.getOccurrences(toFind); 
        /*System.out.println(" sort minimum " + minimum);
        System.out.print("tempWord: ");
        tempWord.print();*/
        this.occurrences.add(tempWord);
        i++;
        for (; i < this.startList.size(); i++) {
            System.out.println("i: " + i + "\nminimum: " + minimum);
            tempWord = this.startList.get(i);
            if (minimum > tempWord.getOccurrences(toFind)) {
                this.occurrences = new ArrayList<Word>();
                this.occurrences.add(tempWord);
                minimum = tempWord.getOccurrences(toFind);
            } else if (minimum == tempWord.getOccurrences(toFind)) {
                this.occurrences.add(tempWord);
            }
        }
        return minimum;
    }
    
    /**
     * Choose will take the result of sort (sorting the available 
     * words by number of occurrances of the letter (before or after 
     * we have chosen the one with lowest number of occurrances?)
     * and organize them based on the position of the occurances, and 
     * finally choose the occurance positions that result in the longest
     * list of words.
     * 
     * @param toFind - char being searched for and ranked based on location
     */
//    public ArrayList<Word> choose(char toFind) {
    public void choose(char toFind) {
        /**
         * Super-ArrayList's length is the equal to the maximum binary
         * representation given the length of the words (if every 
         * letter of the word were the guessed word such that 
         * its binary representation is all ones).  
         * This way it will never need to expand its size.
         */
        
        /*System.out.println("\n\n occurrances !!!!!\n");
        this.occurrences.get(0).print();
        this.occurrences.get(1).print();*/
        
        HashMap<Integer, ArrayList<Word>> byPosition =
                new HashMap<Integer, ArrayList<Word>>();
        
        // Total number of words we are dealing with  
        int tot = this.occurrences.size();
        //System.out.println("inside choose\n tot: " + tot);
        
        
        // Put them all into their respective sub-ArrayLists 
        // (calls letterFinder, this how we wanted?)
        for (int i = 0; i < tot; ++i) {
            
            int locationEncoding = this.occurrences.get(i).letterFinder(toFind);
            
            /*System.out.println(locationEncoding);
            System.out.println("i: " + i);*/
            
            if (!byPosition.containsKey(locationEncoding)) {
                byPosition.put(locationEncoding, new ArrayList<Word>());
            }
            byPosition.get(locationEncoding).add(this.occurrences.get(i)); 
            
        }
        
        /**
         * The number of words that have been counted by below for loop.
         * If (tot - counted) (aka the words you havnt looked at yet) 
         * becomes less than the longest list, you know it is impossible for 
         * another list to be longer therefore you have the longest one
         */
        
        // Index of the longest sub-ArrayList
        int longestKey = 0;
        // Size of the longest sub-ArrayList
        int longestSize = 0;
        
        
        // Go through sub-ArrayLists looking for longest one
        
        for (HashMap.Entry<Integer, ArrayList<Word>> entry
                : byPosition.entrySet()) {
            if (entry.getValue().size() > longestSize) {
                longestKey = entry.getKey();
                longestSize = entry.getValue().size();
            }
                  
        }
        
        //return byPosition.get(longestKey);
        this.startList = byPosition.get(longestKey);
        
    }
    
}

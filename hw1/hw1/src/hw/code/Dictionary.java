package hw.code;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Random;

/**
 * Class that holds the dictionary immediately after it is read in.  
 * Consists of a HashMap that maps an Integer to an ArrayList of Words.
 * In each ArrayList all words are the same length, their length is the 
 * Integer key that maps to their ArrayList in the HashMap.  This is better
 * than an ArrayList of ArrayLists because there are no empty ArrayLists, upon
 * construction only those for which words of that length exist are created.  
 * getWordList() returns a random one of these ArrayLists for gameplay.     
 * @author TKeady
 *
 */
public class Dictionary {
    
    /**
     * Primary struct inside the class. 
     */
    private HashMap<Integer, ArrayList<Word>> dict;
    
    
    
    /**
     * Constructor takes the parameter of the file name holding the dictionary. 
     * It then opens it and begins sorting the words into ArrayLists based on
     * their lengths.  
     * @param fileName the name of the plain text file holding the dictionary.
     */
    public Dictionary(String fileName) {
        
        /**
         * Make the struct.
         */
        this.dict = new HashMap<Integer, ArrayList<Word>>();
        
        /**
         * Has to be inside try - catch block because opening the file can fail.
         * Uses a scanner to grab the word, then puts it into the appropriate 
         * ArrayList, creating that ArrayList first if it does not exist.  
         */
        try {
            
            /**
             * The Scanner.
             */
            Scanner file = new Scanner(new FileReader(fileName));   
            while (file.hasNext()) {
                
                Word word = new Word(file.next());
                //System.out.println(this.dict.containsKey(word.getSize()));

                if (!this.dict.containsKey(word.getSize())) {
                    ArrayList<Word> tempList = new ArrayList<Word>();
                    this.dict.put(word.getSize(), tempList);
                }
                this.dict.get(word.getSize()).add(word);
            }
            file.close();            
            
        } catch (IOException e) {
            System.out.println(e.toString());
            System.out.println("Could not find file " + fileName);
            // NOW WHAT DO?
        }
        

    
    }
    
    /**
     * Returns a random ArrayList<Word> for gameplay.  Choice of ArrayList is
     * completely random and not dependent on word length, ArrayList 
     * length, etc.
     * @return ArrayList<Word> that will be used during the game
     */
    public ArrayList<Word> getWordList() {
        Integer randomInt;
        
        // Have to be careful here to onl grab one that exists
        do {
            //num = (int) Math.floor(Math.random() * this.getSize());  
            
            
            Random random = new Random();
            ArrayList<Integer> keys = 
                    new ArrayList<Integer>(this.dict.keySet());
            randomInt = keys.get(random.nextInt(keys.size()));
            
            //System.out.println(randomInt);
            
            
        } while (!this.dict.containsKey(randomInt));
        
        return this.dict.get(randomInt);
    
    }
    
    
    /**
     * Returns the number of ArrayLists in the dictionary HashMap.
     * Equivalent to the number of different word lengths that exist.  
     * @return int Size of HashMap
     */
    public int getSize() {
        return this.dict.size();
    }
    
    
}
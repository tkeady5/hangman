package hw.code;

import java.util.ArrayList;

/**
 * Class Word is a wrapper for a String with some other
 * useful metadata for the program's use.
 * @author Nitin and Thomas (just Nitin lol -Thomas)
 *
 */

public class Word {
  
    
    /**
     * content is the word that is being held in the wrapper .
     */
    private String content;
    /**
     * occurrences is a alphabet length array list that includes
     * the number of occurrences of each letter in the word. 
     */
    private ArrayList<Integer> occurrences;
    /**
     * alphabet is a variable that is the number of letters in the alphabet.
     */
    private final int alphabet = 26;
  
    /**
    * Word constructor sets content string to the string passed in
    * as well as creates the occurrences ArrayList.
    * @param input - this is the input word that needs to be wrapped
    */
    public Word(String input) {
        this.content = input.toLowerCase();
        this.occurrences = new ArrayList<Integer>(this.alphabet);
        for (int i = 0; i < this.alphabet; i++) {
            this.occurrences.add(0);
        }
        for (int i = 0; i < input.length(); i++) {
            int charVal = (int) this.content.charAt(i)  - (int) 'a';
            //System.out.println(this.content.charAt(i));
            //char is the letter at a specific point in the string
            int oldCount = this.occurrences.get(charVal);
            int newCount = oldCount + 1;
            this.occurrences.set((int) charVal, newCount);
        }
    }
  
    /**
    * Function get size returns length of word.
    * @return length- return length of word
    */
    public int getSize() {
        return this.content.length();
    }
  
    
    /**
     * Prints contents of the word.
     */
    public void print() {
        System.out.print(this.content);
    }
    
    /**
    * getWord returns word itself.
    * @return this.content
    */
    public String getWord() {
        return this.content;
    }
  
    /**
    * getOccurrences returns the number of occurrences
    * has an if statement that acts as a fail safe if 
    * someone inputs an invalid character.
    * @param searchFor - character that is being checked for # occurrences
    * @return occurrences of char being searched for
    */
    public int getOccurrences(char searchFor) {
        
        if (searchFor > 'z' || searchFor < 'a') {
            return 0;
        }
        return this.occurrences.get((int) searchFor - (int) 'a');
    }
  
/**
* letterFinder will search the word for a specific letter.
* It will find all the locations of a given letter in the word
* and will return a number that reflects those locations,
* as if the word is a binary number where every occurrence
* of the given letter is a 1 and every other letter is a 0. 
* @param toFind - letter being searched for
* @return locations - binary encode
*/
    public int letterFinder(char toFind) {
        int locations = 0;
        for (int i = this.content.length() - 1; i >= 0; i--) {
            // start from max length because starting from front of word
            char val = this.content.charAt(i);
            // val is the letter at a specific point in the string;
            if (val == toFind) {
                locations += Math.pow(2, (this.content.length() - i - 1));
            }
        }
        return locations;
    }
  
  
  
}

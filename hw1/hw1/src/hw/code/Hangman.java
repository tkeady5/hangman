package hw.code;
import java.util.Scanner;

/**
 * Main driver of Hangman game for first
 * project of Data Structures.
 * JHED: nkumar14, tkeady1
 * @author Nitin Kumar and Thomas Keady
 *
 */
public final class Hangman {
    
    /**
     * MAX_WRONG is the most wrong guesses you can have
     * before the game is forced ended.
     */
    private static final int MAX_WRONG = 6;
    
    /**
     * Empty constructor to appease checkstyle.
     */
    private Hangman() {
        
    }
    
    /**
     * main driver function for running the Hangman game.
     * @param args - inputs by person in command line
     */
    public static void main(String[] args) {
        System.out.println("Welcome to Hangman!");
        //System.out.println("Input your dictionary:");
        if (0 < args.length) { 
            // DONT FORGET ABOUT DEBUG MODE
            boolean debugMode = false;
            if (1 < args.length) {
                if (args[1].equals("DEBUG")) {
                    debugMode = true;
                }
            }
            Dictionary dictionary = new Dictionary(args[0]);
            System.out.println("Made dictionary successfully");
            //After dictionary is made, initial GameList 
            int numGames = 0;
            int numWins = 0;
            boolean playing = true;
            int whatDo;
            // bool to see if a game is won yet
            boolean victory = false;
            // number of strikes against player; basically
            // how many mistakes they have made
            int strikes = 0;
            /**
             * ONLY ONE SCANNER.  
             */
            Scanner sc = new Scanner(System.in);
            while (playing) {
                whatDo = menuMethod(sc);
                if (whatDo == 1) {
                    WordList gameList = 
                            new WordList(dictionary.getWordList());    
                    //while (!victory) {
                    // validity of input guess
                    boolean validGuess = false;
                    boolean fullWordGuess = false;
                    //String guessInput = "?";
                    Character guessedChar = '?';
                    String guess = "?";
                    String inputString = "?";
                    StringBuffer slots = new StringBuffer(
                            gameList.getWordSize());   // the slots they see
                    for (int i = 0; i < gameList.getWordSize(); i++) {
                        slots.append('_');
                    }
                    StringBuffer wrongs = new StringBuffer(MAX_WRONG);
                    while (((MAX_WRONG - strikes) > 0) 
                            && !victory) {
                        do {
                            if (debugMode) {
                                for (int i = 0; i < gameList.size(); ++i) {
                                    //System.out.println(gameList.get(i));
                                    gameList.get(i).print();
                                    System.out.print("\n");
                                }
                                System.out.println(gameList.size()
                                       + " words currently in play");
                            }    
                            System.out.print("Guess a letter\n\n");
                            System.out.print("\t" + slots 
                                    + "\nIncorrect guesses: " + wrongs);
                            System.out.print("\nYour letter guess: ");       
                            inputString = sc.next().toLowerCase();
                            
                            if (inputString.length() > 1) {
                             // If they guess the whole thing
                                                              
                                if (gameList.get(0).getWord()
                                        .equals(inputString) 
                                        && gameList.size() == 1) {
                                    // GUESSED WORD CORRECTLY
                                    victory = true;
                                    
                                } else {
                                    strikes = MAX_WRONG;
                                }
                                validGuess = true;
                                fullWordGuess = true;
                            } else {
                                fullWordGuess = false;
                                guessedChar = inputString.charAt(0);
                                guess = guessedChar + "";
                                if (Character.isLetter(guessedChar)) {
                                    if (wrongs.indexOf(guessedChar + "")
                                            == -1) {
                                        validGuess = true;
                                    } else {
                                        System.out.println(
                                                "You guessed that already");
                                        validGuess = false;
                                    }                       
                                } else {
                                    validGuess = false;
                                }
                            }
                        } while (!validGuess);   
                        if (!fullWordGuess) {
                            int minOccurrences = 
                                    gameList.sort(guessedChar);            
                            
                            gameList.choose(guessedChar);
                            // this needs to be out here no matter what
                            
                            if (minOccurrences != 0) {
                                // correct guess
                                for (int i = 0; i < gameList.getWordSize();
                                        ++i) {
                                    if (gameList.get(0).getWord().charAt(i) 
                                            == guessedChar) {
                                        slots.setCharAt(i, guessedChar);
                                    }
                                }
                                
                                if (slots.toString()
                                        .equals(gameList.get(0).getWord())) {
                                    // VICTORY
                                    victory = true;
                                }
                                
                            } else {
                                // incorrect guess
                                strikes++;
                                wrongs.append(guessedChar); 
                            }
                        }                    
                    }
                    ++numGames;
                    if (strikes != MAX_WRONG) {
                        ++numWins;
                        System.out.print("Congrats!");
                        System.out.print("\n" + strikes);
                    } else {
                        System.out.print("Game over."); 
                    }
                    // If they were right, gets only word left,
                    // if wrong, just grab first
                    System.out.print("The secret word was ");
                    if (inputString == gameList.get(0).getWord()) {
                        gameList.get(1).print();
                    } else {
                        gameList.get(0).print();                        
                    }
                    System.out.print("\n");
                    // Reset game state
                    victory = false;
                    strikes = 0;
                } else if (whatDo == 2) {
                    System.out.println("Thanks for playing!");
                    System.out.println("You played " + numGames 
                            + " games and won " + numWins + " times.");
                    System.out.println("You also lost "
                            + (numGames - numWins) + " times");
                    System.out.println("Hope you had fun!");
                    playing = false;
                }   
            }      
        } else {
            System.out.println("Please enter the dictionary file"
                    + " as a command-line argument");
        }
    }
    
    /**
     * Indent function for moving around turns.
     */
    public static void gameMenu() {
        System.out.print("\n\t\t");
        
    }
    
    /**
     * menuMethod runs the menu and catches input
     * to decide next action.
     * @param sc - passes in an input reader to not have
     *              2 scanners running at same time
     * @return int - returns which option was chosen
     */
    public static int menuMethod(Scanner sc) {
        System.out.print("\t\tHangman: Main Menu\n*** "
                + "Enter your choice below ***\n\n");
        System.out.print("1: New game\n2. Quit\nEnter choice: ");
        
        //Scanner sc = new Scanner(System.in);
        int i = sc.nextInt(); 
        if (i == 1) {
            //sc.close();
            return 1;
        } else if (i == 2) {
            //sc.close();
            return 2;
        } else {
            System.out.println("Please enter '1' or '2'\n");
            //sc.close();
            return menuMethod(sc);
        }
    }
    
    
    
    
    
    
}
